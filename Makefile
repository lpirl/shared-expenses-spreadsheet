FODS_FILES?=$(wildcard *.fods)
OTS_FILES+=$(FODS_FILES:.fods=.ots)

all: $(OTS_FILES)

%.ots: %.fods
	localc --convert-to ots $?

clean:
	rm $(OTS_FILES)
