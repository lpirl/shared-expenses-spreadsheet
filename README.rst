.. image:: https://gitlab.com/lpirl/shared-expenses-spreadsheet/badges/master/pipeline.svg
  :target: https://gitlab.com/lpirl/shared-expenses-spreadsheet/pipelines
  :align: right

`shared expenses spreadsheet template <https://gitlab.com/lpirl/shared-expenses-spreadsheet/-/jobs/artifacts/master/raw/shared-expenses.ots?job=templates>`__
=============================================================================================================================================================

(↑↑↑ click the link to download ↑↑↑)

This is a simple spreadsheet to split/share expenses among multiple
buyers. Each item bought can be split in unequal shares.

We use the flat XML open document format (``.fods``) to be more
compatible with version control. The corresponding open document
template is generated using the CI.

Feel free to suggest changes.
